/**
 * 
 */
package com.aws.github.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aws.github.model.Instance;

/**
 * @author Dhruv Bansal
 *
 */
public class InstanceRowMapper implements RowMapper<Instance> {
  @Override
  public Instance mapRow(ResultSet rs, int rowNum) throws SQLException {
    if (rs.getRow() == 0) {
      return null;
    }
    Instance instance = new Instance();
    instance.setId(rs.getString("instance_id"));
    return instance;
  }
}


