/**
 * 
 */
package com.aws.github.repository.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.aws.github.model.SecurityGroup;

/**
 * @author Dhruv Bansal
 *
 */
public class GroupRowMapper implements RowMapper<SecurityGroup> {
  @Override
  public SecurityGroup mapRow(ResultSet rs, int rowNum) throws SQLException {
    if (rs.getRow() == 0) {
      return null;
    }
    SecurityGroup group = new SecurityGroup();
    group.setId(rs.getString("group_id"));
    return group;
  }
}


