/**
 * 
 */
package com.aws.github.exceptions;

/**
 * @author Dhruv Bansal
 *
 */
public class BadRequestException extends Exception{
  
  public BadRequestException() {
    super();
  }

}
