package com.aws.github.services;

import java.util.List;

import com.aws.github.model.Credential;
import com.aws.github.model.Instance;
import com.aws.github.model.SecurityGroup;

public interface AwsService {

  List<Instance> getGroupInstances(String groupId);
//
//  void updateAvatarURL(AwsMapping actor) throws NotFoundException, BadRequestException;
//
//  List<AwsMapping> getActorsByEventCount();
//
//  void erase();

  /**
   * @param instanceId
   * @return
   */
  List<SecurityGroup> getInstanceGroups(String instanceId);

  /**
   * Credentials having
   * @param aws_access_key_id
   * @param aws_secret_access_key
   * @param region
   */
  void saveAndUpdateMapping(Credential credentials);

  /**
   * 
   */
  void erase();

}
