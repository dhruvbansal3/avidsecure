package com.aws.github.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.GroupIdentifier;
import com.amazonaws.services.ec2.model.Reservation;
import com.aws.github.model.AwsMapping;
import com.aws.github.model.Credential;
import com.aws.github.model.Instance;
import com.aws.github.model.SecurityGroup;
import com.aws.github.repository.AWSRepository;
import com.aws.github.services.AwsService;

@Service
public class AwsServiceImpl implements AwsService{

  @Autowired
  private AWSRepository awsRepository;

  @Override
  public List<Instance> getGroupInstances(String groupId) {
    return awsRepository.getGroupInstances(groupId);    
  }

  @Override
  public List<SecurityGroup> getInstanceGroups(String instanceId) {
    return awsRepository.getInstanceGroups(instanceId);    
  }
  
  
  @Override
  public void saveAndUpdateMapping(Credential credentials) {
    BasicAWSCredentials awsCreds = new BasicAWSCredentials(credentials.getAws_access_key_id(), credentials.getAws_secret_access_key());
    final AmazonEC2 ec2 = AmazonEC2ClientBuilder.standard()
                          .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                          .withRegion(credentials.getRegion())
                          .build();
    DescribeInstancesRequest request = new DescribeInstancesRequest();
    boolean done = false;
    List<AwsMapping> mappings = new ArrayList<AwsMapping>();
    while(!done) {
        DescribeInstancesResult response = ec2.describeInstances(request);

        for(Reservation reservation : response.getReservations()) {
            for(com.amazonaws.services.ec2.model.Instance instance : reservation.getInstances()) {
              List<GroupIdentifier> groups = instance.getSecurityGroups();
              
              for (GroupIdentifier group : groups) {
                mappings.add(new AwsMapping(instance.getInstanceId(), group.getGroupId()));                
              }

              
              
                System.out.printf(
                    "Found instance with id %s, " +
                    "AMI %s, " +
                    "type %s, " +
                    "state %s " +
                    "and monitoring state %s",
                    instance.getInstanceId(),
                    instance.getImageId(),
                    instance.getInstanceType(),
                    instance.getState().getName(),
                    instance.getMonitoring().getState());
            }
        }

        request.setNextToken(response.getNextToken());

        if(response.getNextToken() == null) {
            done = true;
        }
    }
    awsRepository.saveBatch(mappings);
  }
  
  @Override
  public void erase() {
    awsRepository.erase();
  }
}
