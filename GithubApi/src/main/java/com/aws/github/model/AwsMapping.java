package com.aws.github.model;

public class AwsMapping {
  private String instance_id;
  private String group_id;

  public AwsMapping(String instance_id, String group_id) {
    this.instance_id = instance_id;
    this.group_id = group_id;
  }

  public String getInstance_id() {
    return instance_id;
  }

  public void setInstance_id(String instance_id) {
    this.instance_id = instance_id;
  }

  public String getGroup_id() {
    return group_id;
  }

  public void setGroup_id(String group_id) {
    this.group_id = group_id;
  }

}
