package com.aws.github.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aws.github.model.Credential;
import com.aws.github.model.Instance;
import com.aws.github.model.SecurityGroup;
import com.aws.github.services.AwsService;

@RestController
public class GithubApiRestController {


  @Autowired
  AwsService awsService;

  @RequestMapping(value = "/saveAwsMappings", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public ResponseEntity<Void> addEvent(@RequestBody Credential credentials) {
    awsService.erase();
    awsService.saveAndUpdateMapping(credentials);
    return new ResponseEntity<Void>(HttpStatus.CREATED);
  }

  @RequestMapping(value = "/erase", method = RequestMethod.DELETE)
  public ResponseEntity<Void> eraseAllEvents() {
    awsService.erase();
    return new ResponseEntity<Void>(HttpStatus.OK);
  }

  @RequestMapping(value = "/instances/{groupID}", method = RequestMethod.GET, produces = "application/json")
  public ResponseEntity<List<Instance>> getInstances(
      @PathVariable(value = "groupID") String groupID) {
    return new ResponseEntity<List<Instance>>(awsService.getGroupInstances(groupID), HttpStatus.OK);
  }

  @RequestMapping(value = "/groups/{instanceID}", method = RequestMethod.GET, produces = "application/json")
  public ResponseEntity<List<SecurityGroup>> getGroups(
      @PathVariable(value = "instanceID") String instanceID) {
    return new ResponseEntity<List<SecurityGroup>>(awsService.getInstanceGroups(instanceID), HttpStatus.OK);
  }

}
